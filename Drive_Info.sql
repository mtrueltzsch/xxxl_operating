

USE BI_DWH_OPS
GO

SET NOCOUNT ON
DECLARE @hr int
DECLARE @fso int
DECLARE @drive char(1)
DECLARE @odrive int
DECLARE @TotalSize varchar(20)
DECLARE @MB numeric;
SET @MB = 1048576
/*
CREATE TABLE #drives (
  drive char(1) PRIMARY KEY,
  FreeSpace int NULL,
  TotalSize int NULL
)
*/
INSERT INTO BI_DWH_OPS.dbo.Drive_Info(Drive, Free_MB) 

EXEC master.dbo.xp_fixeddrives
EXEC @hr = sp_OACreate 'Scripting.FileSystemObject',
                       @fso OUT
IF @hr <> 0
  EXEC sp_OAGetErrorInfo @fso
DECLARE dcur CURSOR LOCAL FAST_FORWARD FOR
SELECT
  Drive
FROM BI_DWH_OPS.dbo.Drive_Info
ORDER BY Drive
OPEN dcur
FETCH NEXT FROM dcur INTO @drive
WHILE @@FETCH_STATUS = 0
BEGIN
  EXEC @hr = sp_OAMethod @fso,
                         'GetDrive',
                         @odrive OUT,
                         @drive
  IF @hr <> 0
    EXEC sp_OAGetErrorInfo @fso
  EXEC @hr =
  sp_OAGetProperty @odrive,
                   'TotalSize',
                   @TotalSize OUT
  IF @hr <> 0
    EXEC sp_OAGetErrorInfo @odrive
  UPDATE BI_DWH_OPS.dbo.Drive_Info
  SET Total_MB = @TotalSize / @MB
  WHERE Drive = @drive
  FETCH NEXT FROM dcur INTO @drive
END
CLOSE dcur
DEALLOCATE dcur
EXEC @hr = sp_OADestroy @fso
IF @hr <> 0
  EXEC sp_OAGetErrorInfo @fso
SELECT
  Drive,
  Total_MB AS 'Total(MB)',
  Free_MB AS 'Free(MB)'
FROM BI_DWH_OPS.dbo.Drive_Info
ORDER BY Drive
--DROP TABLE #drives
GO