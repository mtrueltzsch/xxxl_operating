/*
Aufgabenstellung:
- Dynamisch f�r jede Tabelle bzw. Datenbank ausf�hren
- Ergebnisse in eine Tabelle schreiben
*/


DECLARE
@databaseID nvarchar(10),
@database_name nvarchar(50),
@sql nvarchar(max)
--@temp_sql TABLE ( TableName NVARCHAR(MAX))


DECLARE Table_Info_Cursor CURSOR FOR

SELECT 
database_id,
name
FROM sys.databases;

OPEN Table_Info_Cursor;
FETCH Table_Info_Cursor INTO @databaseID, @database_name;

WHILE @@FETCH_STATUS = 0 
BEGIN

SET @database_name = (SELECT name FROM sys.databases WHERE database_id = @databaseID) 


SET @sql = 
'

INSERT INTO [BI_DWH_OPS].[dbo].[Table_Info]
([Table_Name], [RowCount], [Count_Index_ID], [ReservedSize_KB], [DataSize_KB], [IndexSize_KB], [UnusedSize_KB], [ReservedSize_MB], [DataSize_MB], [IndexSize_MB], [UnusedSize_MB], [ReservedSize_GB], [DataSize_GB], [IndexSize_GB], [UnusedSize_GB])

SELECT
    a2.name AS TableName,
    a1.rows as [RowCount],
	a1.Count_Index_ID,
	--KB
    (a1.reserved + ISNULL(a4.reserved,0)) * 8 AS ReservedSize_KB,
    a1.data * 8 AS DataSize_KB,
    (CASE WHEN (a1.used + ISNULL(a4.used,0)) > a1.data THEN (a1.used + ISNULL(a4.used,0)) - a1.data ELSE 0 END) * 8 AS IndexSize_KB,
    (CASE WHEN (a1.reserved + ISNULL(a4.reserved,0)) > a1.used THEN (a1.reserved + ISNULL(a4.reserved,0)) - a1.used ELSE 0 END) * 8 AS UnusedSize_KB,
	--MB
    CAST(ROUND(((a1.reserved + ISNULL(a4.reserved,0)) * 8) / 1024.00, 2) AS NUMERIC(36, 2)) AS ReservedSize_MB,
    CAST(ROUND(a1.data * 8 / 1024.00, 2) AS NUMERIC(36, 2)) AS DataSize_MB,
    CAST(ROUND((CASE WHEN (a1.used + ISNULL(a4.used,0)) > a1.data THEN (a1.used + ISNULL(a4.used,0)) - a1.data ELSE 0 END) * 8 / 1024.00, 2) AS NUMERIC(36, 2)) AS IndexSize_MB,
    CAST(ROUND((CASE WHEN (a1.reserved + ISNULL(a4.reserved,0)) > a1.used THEN (a1.reserved + ISNULL(a4.reserved,0)) - a1.used ELSE 0 END) * 8 / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSize_MB,
    --GB
    CAST(ROUND(((a1.reserved + ISNULL(a4.reserved,0)) * 8) / 1024.00 / 1024.00, 2) AS NUMERIC(36, 2)) AS ReservedSize_GB,
    CAST(ROUND(a1.data * 8 / 1024.00 / 1024.00, 2) AS NUMERIC(36, 2)) AS DataSize_GB,
    CAST(ROUND((CASE WHEN (a1.used + ISNULL(a4.used,0)) > a1.data THEN (a1.used + ISNULL(a4.used,0)) - a1.data ELSE 0 END) * 8 / 1024.00 / 1024.00, 2) AS NUMERIC(36, 2)) AS IndexSize_GB,
    CAST(ROUND((CASE WHEN (a1.reserved + ISNULL(a4.reserved,0)) > a1.used THEN (a1.reserved + ISNULL(a4.reserved,0)) - a1.used ELSE 0 END) * 8 / 1024.00 / 1024.00, 2) AS NUMERIC(36, 2)) AS UnusedSize_GB
FROM
    (SELECT 
        ps.object_id,
		COUNT(ps.index_id) Count_Index_ID,
        SUM (CASE WHEN (ps.index_id < 2) THEN row_count ELSE 0 END) AS [rows],
        SUM (ps.reserved_page_count) AS reserved,
        SUM (CASE
                WHEN (ps.index_id < 2) THEN (ps.in_row_data_page_count + ps.lob_used_page_count + ps.row_overflow_used_page_count)
                ELSE (ps.lob_used_page_count + ps.row_overflow_used_page_count)
            END
            ) AS data,
        SUM (ps.used_page_count) AS used
    FROM ' + @database_name + '.sys.dm_db_partition_stats ps
    GROUP BY ps.object_id) AS a1
LEFT OUTER JOIN 
    (SELECT 
        it.parent_id,
        SUM(ps.reserved_page_count) AS reserved,
        SUM(ps.used_page_count) AS used
     FROM ' + @database_name + '.sys.dm_db_partition_stats ps
     INNER JOIN ' + @database_name + '.sys.internal_tables it ON (it.object_id = ps.object_id)
     WHERE it.internal_type IN (202,204)
     GROUP BY it.parent_id) AS a4 ON (a4.parent_id = a1.object_id)
INNER JOIN ' + @database_name + '.sys.all_objects a2  ON ( a1.object_id = a2.object_id ) 
INNER JOIN ' + @database_name + '.sys.schemas a3 ON (a2.schema_id = a3.schema_id)
WHERE a2.type <> N''V'' and a2.type <> N''U''
--AND a2.name = ''Testtabelle_MT''      
ORDER BY ReservedSize_MB DESC
'

PRINT @sql
--EXEC (@sql)
--EXECUTE sp_executesql @sql

 FETCH Table_Info_Cursor INTO @databaseID, @database_name;
  
  END;


CLOSE Table_Info_Cursor;
DEALLOCATE Table_Info_Cursor;


----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
-- Testaufrufe
SELECT * FROM sys.databases
SELECT * FROM sys.internal_tables
SELECT * FROM sys.tables
SELECT * FROM sys.schemas
SELECT * FROM sys.all_objects


-- Welche Arten von Objekten sollen gepr�ft werden -> Views & Tabellen
SELECT DISTINCT
o.type,
o.type_desc
FROM sys.all_objects o
--where o.type like 'IT' 
*/

